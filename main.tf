# Azure Provider source and version being used
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.99.0"
    }
  }
}
# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
  # Configuration options
}

#EVENTHUB NAMESPACE
resource "azurerm_eventhub_namespace" "namespace_name" {
  name                = var.eventhub_namespace.name
  location            = var.eventhub_namespace.location
  resource_group_name = var.eventhub_namespace.resource_group_name
  sku                 = var.eventhub_namespace.sku
  capacity            = var.eventhub_capacity
  tags                = var.eventhub_tags
}

#SHARED ACCESS POLICY
resource "azurerm_eventhub_namespace_authorization_rule" "policy-teste" {
  name                = var.policy_name
  namespace_name      = var.eventhub_namespace.name
  resource_group_name = var.eventhub_namespace.resource_group_name

  listen = var.policy_options.listen
  send   = var.policy_options.send
  manage = var.policy_options.manage
}

#EVENTHUB TOPICS
resource "azurerm_eventhub" "topic-teste" {
  name                = "topic-teste"
  namespace_name      = var.eventhub_namespace.name
  resource_group_name = var.eventhub_namespace.resource_group_name
  partition_count     = var.eventhub_topic_partition_count
  message_retention   = var.eventhub_topic_message_retention
}

#DADOS DO NAMESPACE
variable "eventhub_namespace" {
  description = "Dados do namespace"
  type        = map(string)
  default = {
    name                = "teste"
    location            = "brazilsouth"
    resource_group_name = "RSG_TESTE"
    sku                 = "Standard"
  }
}

#DADOS DO NAMESPACE
variable "eventhub_capacity" {
  description = "Capacidade"
  type        = number
  default     = "1"
}

#TAGS DO NAMESPACE
variable "eventhub_tags" {
  description = "Tags"
  type        = map(string)
  default = {
    environment = "qual o env"
    squad       = "Nome da squad"
  }
}

#NOME DA SHARED ACCESS POLICY
variable "policy_name" {
  description = "Nome da shared access policy"
  type        = string
  default     = "policy-teste"
}

#SHARED ACCESS POLICY OPTIONS
variable "policy_options" {
  description = "Opcoes da policy"
  type        = map(bool)
  default = {
    listen = true
    send   = true
    manage = false

  }
}

#Message Retention do topico
variable "eventhub_topic_message_retention" {
  description = "Message Retention do topico"
  type        = number
  default     = "1"
}

#Pattition Vount do topico
variable "eventhub_topic_partition_count" {
  description = "Pattition Vount do topico"
  type        = number
  default     = "1"
}
